local fs = require("filesystem")
local shell = require("shell")
local zlib = require("zlib")

-- [[ Utilities ]] --

local function todo()
  error("unimplemented")
end

local function isNotNull(c)
  return c ~= "\0"
end

local function isValidOid(oid)
  -- make sure the oid is lowercase
  return oid:match("^" .. ("[0-9a-f]"):rep(40) .. "$")
end

-- [[ Parsing ]] --

local createStream do
  local function isNotNewline(c)
    return c ~= "\n"
  end

  local meta = {
    __index = {
      peek = function(self, n)
        if n then
          return self.str:sub(self.pos, self.pos + n - 1)
        end

        return self.str:sub(self.pos)
      end,

      read = function(self, n)
        local result = self:peek(n)
        self.pos = math.min(#self.str, self.pos + #result - 1)

        return result
      end,

      readLine = function(self)
        return self:readWhile(isNotNewline)
      end,

      readWhile = function(self, predicate)
        local result = ""

        while true do
          local c = self:peek(1)

          if not predicate(c) then
            return result
          end

          result = result .. self:read(1)
        end
      end,

      consume = function(self, str)
        return self:read(#str) == str
      end,

      expect = function(self, str)
        return self:peek(#str) == str
      end,

      isEmpty = function(self)
        return #self == 0
      end,
    },

    __len = function(self)
      return #self.str - self.pos + 1
    end,
  }

  function createStream(str)
    return setmetatable({
      str = str,
      pos = 1,
    }, meta)
  end
end

local objectParsers = {}

local function parseTreeEntryFlags(stream)
  local result = {}
  local fileType = stream:read(2)

  if fileType == "10" or fileType == "12" then
    -- consume one more byte (we expect 100 or 120)
    fileType = fileType .. stream:read(1)
  end

  if fileType == "100" then
    result.type = "file"
  elseif fileType == "120" then
    result.type = "link"
  elseif fileType == "40" then
    result.type = "dir"
  else
    return nil, "unknown file type"
  end

  local perms = stream:read(3)

  if result.type == "file" then
    if perms == "644" then
      result.executable = false
    elseif perms == "755" then
      result.executable = true
    else
      return nil, "expected either 644 or 755 as the file mode"
    end
  elseif perms ~= "000" then
    return nil, "expected 000 as the file mode"
  end

  return result
end

local function parseTreeEntry(stream)
  local flags, err = parseTreeEntryFlags(stream)

  if not flags then
    return nil, "corrupted file: " .. err
  end

  if not stream:consume(" ") then
    return nil, "corrupted file: expected whitespace"
  end

  local fileName = stream:readWhile(isNotNull)

  if not stream:consume("\0") then
    return nil, "corrupted file: expected a null terminator"
  end

  local objectHash = stream:read(20)

  if #objectHash ~= 20 then
    return nil, "corrupted file: met an abrupt file end"
  end

  return {
    type = flags.type,
    executable = flags.executable,
    name = fileName,
    objectHash = objectHash,
  }
end

function objectParsers:tree(stream)
  local entries = {}

  repeat
    local entry, err = parseTreeEntry(stream)

    if not entry then
      return nil, err
    end

    table.insert(entries, entry)
  until stream:isEmpty()

  return entries
end

local function readEntry(stream, tag)
  if not stream:consume(tag .. " ") then
    return nil, "corrupted file: expected a " .. tag .. "entry"
  end

  local data = stream:readLine()

  if not stream:consume("\n") then
    return nil, "corrupted file: expected a newline"
  end

  return data
end

local function readOidEntry(stream, tag)
  local oid, err = readEntry(stream, tag)

  if not oid then
    return nil, "corrupted file: " .. err
  end

  if not isValidOid(oid) then
    return nil, "corrupted file: expected an object id"
  end

  return oid
end

local function readObjectMessage(stream)
  if not stream:consume("\n") then
    return nil, "corrupted file: expected an introductory newline"
  end

  return stream:read()
end

do
  local function readTreeEntry(stream)
    return readOidEntry(stream, "tree")
  end

  local function readParentEntry(stream)
    return readOidEntry(stream, "parent")
  end

  local function readAuthorEntry(stream)
    -- treat as opaque for now
    return readEntry(stream, "author")
  end

  local function readCommitterEntry(stream)
    -- treat as opaque for now
    return readEntry(stream, "committer")
  end

  local function readGpgSignatureEntry(stream)
    local signatureLines = {}

    if not stream:consume("gpgsig ") then
      return nil, "corrupted file: expected a gpg signature tag"
    end

    repeat
      table.insert(stream:readLine())

      if not stream:consume("\n") then
        return nil, "corrupted file: expected a newline"
      end
    until stream:expect("\n") or stream:isEmpty()

    return table.concat(signatureLines, "\n")
  end

  function objectParsers:commit(stream)
    local result = {}
    local err
    result.treeOid, err = readTreeEntry(stream)

    if not result.treeOid then
      return nil, err
    end

    result.parents = {}

    while stream:peek(6) == "parent" do
      local oid, err = readParentEntry(stream)

      if not oid then
        return nil, err
      end

      table.insert(result.parents, oid)
    end

    if stream:peek(6) == "author" then
      result.author, err = readAuthorEntry(stream)

      if not result.author then
        return nil, err
      end
    end

    if stream:peek(9) == "committer" then
      result.committer, err = readCommitterEntry(stream)

      if not result.committer then
        return nil, err
      end
    end

    if stream:peek(6) == "gpgsig" then
      result.gpgSignature, err = readGpgSignatureEntry(stream)

      if not result.gpgSignature then
        return nil, err
      end
    end

    result.message, err = readObjectMessage(stream)

    if not result.message then
      return nil, err
    end

    return result
  end
end

function objectParsers:blob(stream)
  return stream:read()
end

do
  local function readObjectTypeEntry(stream)
    local objectType, err = readEntry(stream, "object")

    if not objectType then
      return nil, err
    end

    -- a quite and dirty check for object type validity
    if objectParsers[objectType] then
      return objectType
    end

    return nil, "corrupted file: invalid object type"
  end

  local function readTagNameEntry(stream)
    return readEntry(stream, "tag")
  end

  local function readTaggerEntry(stream)
    return readEntry(stream, "tagger")
  end

  function objectParsers:tag(stream)
    local result = {}
    local err

    result.oid, err = readOidEntry(stream, "object")

    if not result.oid then
      return nil, err
    end

    result.objectType, err = readObjectTypeEntry(stream)

    if not result.objectType then
      return nil, err
    end

    result.tagName, err = readTagNameEntry(stream)

    if not result.tagName then
      return nil, err
    end

    if stream:peek("tagger") then
      result.tagger, err = readTaggerEntry(stream)

      if not result.tagger then
        return nil, err
      end
    end

    result.message, err = readObjectMessage(stream)

    if not result.message then
      return nil, err
    end

    return result
  end
end

local function parseObjectHeader(stream)
  local objectType

  if stream:peek(3) == "tag" then
    objectType = stream:read(3)
  elseif stream:peek(4) == "tree" then
    objectType = stream:read(4)
  elseif stream:peek(4) == "blob" then
    objectType = stream:read(4)
  elseif stream:peek(6) == "commit" then
    objectType = stream:read(6)
  else
    return nil, "unknown object type"
  end

  if not stream:consume(" ") then
    return nil, "corrupted file"
  end

  local size = tonumber(stream:readWhile(isNotNull), 10)

  if not size then
    return nil, "corrupted file"
  end

  if not stream:consume("\0") then
    return nil, "corrupted file"
  end

  return objectType, size
end

local function parseObject(objectData)
  local stream = createStream(objectData)
  local objectType, size = parseObjectHeader(stream)

  if not objectType then
    return nil, size
  end

  if size ~= #stream then
    return nil, "invalid size"
  end

  local object = objectParsers[objectType](stream)
  object.type = objectType

  return object
end

-- [[ Filesystem stuff ]] --

local function getCurrentDirectory()
  return shell.getWorkingDirectory() or os.getenv("PWD")
end

local function canonicalizePath(path)
  local absolutePath = shell.resolve(path)

  return fs.canonical(absolutePath)
end

-- Look for the repository root, ascending from cwd
local function findRepositoryRoot()
  local cwd = getCurrentDirectory()

  if not cwd then
    -- can this really happen?..
    -- better safe than sorry, anyway
    return nil, "could not find the current directory"
  end

  local basePath = canonicalizePath(cwd)

  repeat
    local possibleGitLocation = fs.concat(basePath, ".git/")

    if fs.isDirectory(possibleGitLocation) then
      return possibleGitLocation
    end

    basePath = fs.concat(basePath, "..")
  until basePath == "/"

  return nil, "could not find the repository root"
end

-- [[ Reading actual files ]] --

local function readObject(path)
  local f = assert(io.open(path, "rb"))
  local compressedData = f:read("*a")
  f:close()

  return zlib.inflate(compressedData)
end

-- [[ Object database ]] --

local newOdb do
  local meta = {
    __index = {
      __getObjectPath = function(self, oid)
        assert(isValidOid(oid))

        return fs.concat(self.root, "objects", oid:sub(1, 2), oid:sub(3))
      end,

      __findStandaloneObject = function(self, oid)
        local path = self:__getObjectPath(oid)

        if not fs.exists(path) then
          return false
        end

        if fs.isDirectory(path) then
          return nil, "corrupted repository"
        end

        return path
      end,

      locate = function(self, oid)
        local path, err = self:__findStandaloneObject(oid)

        if path then
          return {
            type = "standalone",
            path = path,
          }
        end

        if path == false then
          -- TODO: look in the pack files
        end

        if path == nil then
          return nil, err
        end

        return false
      end,

      getByOid = function(self, oid)
        local location, err = self:locate(oid)

        if not location then
          -- false or nil
          return location, err
        end

        local objectContents

        if location.type == "standalone" then
          objectContents, err = readObject(location.path)

          if not objectContents then
            return nil, err
          end
        elseif location.type == "pack" then
          todo()
        else
          error("this was supposed to be unreachable")
        end

        return parseObject(objectContents)
      end,
    },
  }

  function newOdb(repoRoot)
    return setmetatable({
      root = repoRoot,
    }, meta)
  end
end
